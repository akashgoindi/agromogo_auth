import axios from 'axios';
import { error_handler } from "../../handlers/errorHandler";
import { Actions } from "react-native-router-flux";

import {base_url, default_header} from "../../config";
import { LOGIN, LOGOUT, REGISTER, CLEAR_AUTH_MESSAGE } from "../../actionTypes";
import { usersList } from "../../data";
import { requestApiAsync } from '../../handlers/networkHandler';
import { setToken, clearAsyncStorage } from '../../handlers/tokenHandler';
import { Platform } from 'react-native';

let newRegistration = true;


export const login = (email: string, password: string) => {
  let data = { email, password };
  return async(dispatch: Function) => {
    try {
      let res = await requestApiAsync({ path: "authentication/login", method: "POST", params: data });
      await setToken(res);
      dispatch({
        type: LOGIN,
        payload: {
          status: "success",
          data: { token: res },
          message: "Login Successful!"
        }
      });
    } catch(error) {
      dispatch({
        type: LOGIN,
        payload: { 
          status: "error",
          message: "Authentication Error!" 
        }
      })
    }
  }
}

export const register = (data: any) => {
  const { email } = data;
  let token = "Bearer eysjnskajk34kn2lj34n23kj4njk2nm324nj2b3kj2n34j24n3jl234";
  return async (dispatch: Function) => {
    usersList.map((user) => {
      if (user.email === email) {
        newRegistration = false;
      }
    })
    if(newRegistration) {
      await setToken(token);
      dispatch({
        type: REGISTER,
        payload: {
          status: "success",
          data: { token: token },
          message: "Registration Successful!"
        }
      });
    } else {
      dispatch({
        type: REGISTER,
        payload: {
          status: "error",
          message: "User Alreasy Exists! Try Different Email Id"
        }
      })
    }
  }
}

export const logout = () => {
  return (dispatch: Function) => {
    dispatch({
      type: LOGOUT,
      payload: {}
    })
    clearAsyncStorage();
    Actions.login();
  }
}

export const clearAuthMessage = () => {
  return (dispatch: Function) => {
    dispatch({ type: CLEAR_AUTH_MESSAGE })
  }
}
