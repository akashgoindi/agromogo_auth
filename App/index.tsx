import React from "react";
import { Text, View } from "react-native";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";

import Routers from "./routers";
import Reducers from "./reducers";

const App = () => {
	let store = createStore(Reducers, {}, applyMiddleware(ReduxThunk));
	return (
		<Provider store={store}>
			<Routers />
		</Provider>
	);
}

export default App;