import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import {
  View,
  Button,
  Text,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  BackHandler,
} from "react-native";

import { Actions } from 'react-native-router-flux';

import Layout from "../layout/layout";
import { logout } from "../../actions/auth/index";
import { ScrollView } from 'react-native-gesture-handler';

interface IHomeProps {
  logout: Function,
  access_token: string
}

interface IHomeState {
  access_token: string,
  backClickCount: number
}


class HomeScreen extends Component<IHomeProps, IHomeState> {
  constructor(props: IHomeProps) {
    super(props);
    this.state = {
      access_token: "",
      backClickCount: 0
    }
  }

  render() {
    return (
      <Layout pageTitle="Settings">
        <View style={{ marginTop: 40 }}>
          <Text>This is Settings View</Text>
        </View>
      </Layout>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
  }
});

const mapStateToProps = ({ auth }: any) => {
  return ({
    access_token: auth.access_token
  });
}

export default connect(
  mapStateToProps,
  { logout }
)(HomeScreen);