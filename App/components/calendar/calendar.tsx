import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import {
  View,
  Button,
  Text,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  BackHandler,
} from "react-native";

import { Actions } from 'react-native-router-flux';

import Layout from "../layout/layout";
import { logout } from "../../actions/auth/index";
import { ScrollView } from 'react-native-gesture-handler';

interface IHomeProps {
  logout: Function,
  access_token: string
}

interface IHomeState {
  access_token: string,
  backClickCount: number
}


class HomeScreen extends Component<IHomeProps, IHomeState> {
  constructor(props: IHomeProps) {
    super(props);
    this.state = {
      access_token: "",
      backClickCount: 0
    }
  }

  componentDidMount = () => {
    console.log("ComponentDidMount Ran")
  }

  componentWillUnmount = () => {
    console.log("ComponentWillUnmountMount Ran")
  }

  componentDidUpdate = (prevProps: IHomeProps) => {
    if (this.props.access_token !== prevProps.access_token) {
      this.setState({
        access_token: this.props.access_token
      }, () => {
        !this.state.access_token ? Actions.login() : null;
      })
    }
  }

  render() {
    return (
      <Layout pageTitle="Calendar">
        <View style={{ marginTop: 40 }}>
          <Text>This is Calendar View</Text>
        </View>
      </Layout>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
  }
});

const mapStateToProps = ({ auth }: any) => {
  return ({
    access_token: auth.access_token
  });
}

export default connect(
  mapStateToProps,
  { logout }
)(HomeScreen);