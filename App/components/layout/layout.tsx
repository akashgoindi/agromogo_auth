import React, { Component, Fragment } from "react";
import { Actions } from 'react-native-router-flux';
import { View, Text, StatusBar, Image } from "react-native";
import {
  Drawer, Container, Content, Header, Left, Body, Right, 
  Button, Icon, Title, Footer, FooterTab, List, ListItem 
} from 'native-base';

const routes = [
  { title: "Home", action: "home" }, 
  { title: "Calendar", action: "calendar" }
];

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state={};
  }

  openDrawer = () => { this.drawer._root.open() };

  closeDrawer = () => {
    this.drawer._root.close()
  };

  drawerScreen = () => {
    return(
      <View style={{height: "100%", backgroundColor: "#fff"}}>
        <View>
          <View style={{ marginTop: StatusBar.currentHeight }}>
            <Image
              source={{
                uri:
                  "https://raw.githubusercontent.com/GeekyAnts/NativeBase-KitchenSink/master/assets/drawer-cover.png"
              }}
              style={{
                height: 120,
                width: "100%",
                alignSelf: "stretch",
                position: "absolute"
              }}
            />
            <Image
              square
              style={{
                height: 80,
                width: 70,
                position: "absolute",
                alignSelf: "center",
                top: 20
              }}
              source={{
                uri:
                  "https://raw.githubusercontent.com/GeekyAnts/NativeBase-KitchenSink/master/assets/logo.png"
              }}
            />
            <List
              dataArray={routes}
              contentContainerStyle={{ marginTop: 120 }}
              renderRow={(data, index) => {
                return (
                  <ListItem
                    key={index}
                    button
                    onPress={() => {
                      Actions[data.action]()
                    }}
                  >
                    <Text>{data.title}</Text>
                  </ListItem>
                );
              }}
            />
          </View>
        </View>
      </View>
      
    )
  }

  headerComponent = () => {
    return(
      <Header transparent>
        <Left>
          <Button transparent onPress={this.openDrawer}>
            <Icon ios='ios-menu' android="md-menu" style={{ color: "#000" }} name='menu' />
          </Button>
        </Left>
        <Body>
          <Title style={{color: "#000"}}>{this.props.pageTitle}</Title>
        </Body>
      </Header>
    )
  }

  footerComponent = () => {
    return (
      <Footer>
        <FooterTab style={{ backgroundColor: '#fff' }}>
          <Button vertical onPress={() => Actions.home()}>
            <Icon name="home" />
            <Text>Home</Text>
          </Button>
          <Button vertical onPress={()=>Actions.calendar()}>
            <Icon name="calendar" />
            <Text>Calendar</Text>
          </Button>
          <Button vertical>
            <Icon name="navigate" />
            <Text>Navigate</Text>
          </Button>
          <Button vertical>
            <Icon name="settings" />
            <Text>Settings</Text>
          </Button>
        </FooterTab>
      </Footer>
    )
  }

  render() {
    return (
      <Fragment>
        <StatusBar backgroundColor="#2A4365" barStyle="light-content" />
        <Drawer 
          ref={(ref) => { this.drawer = ref; }} 
          content={this.drawerScreen()} 
          onClose={() => this.closeDrawer()} > 
          <Container>
            {this.headerComponent()}
            <Content>
              {this.props.children}
            </Content>
            {this.footerComponent()}
          </Container>
        </Drawer> 
      </Fragment>
    ); 
  }
}

export default Layout;