import { Dimensions, StyleSheet } from 'react-native';
const { height } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    height: height
  },
  splash: {
    height: "80%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#2A4365",
  },
  iconDiv: {
    height: '20%',
    paddingTop: "10%",
    alignItems: "center",
    justifyContent: "center"
  },
  name: {
    fontSize: 22,
    fontWeight: "bold",
    color: "#fff"
  }
});