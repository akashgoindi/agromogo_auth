import React, { Component, Fragment } from 'react';
import { Actions } from "react-native-router-flux";
import { Text, View, StatusBar, Image } from 'react-native';
import { getToken } from '../../handlers/tokenHandler';

import { styles } from "./splashStyles"; 

class SplashScreen extends Component {

  componentDidMount = () => {
    this.nextScreen();
  }

  nextScreen = () => {
    setTimeout(async() => {
      let token = await getToken();
      token ? Actions.home() : Actions.login();
    }, 1500);
  }

  render() {
    return(
      <Fragment>
        <StatusBar backgroundColor="#2A4365" barStyle="light-content" />
        <View style={styles.container}>
          <View style={styles.splash}>
            <View style={{flexDirection:"row", justifyContent: "flex-end", alignItems: "flex-end"}}>
              <Image
                source={require('../../assets/icons/alpha_a.png')}
              />
              <Text style={styles.name}>GROMOJO</Text>
            </View>
          </View>
          <View style={styles.iconDiv}>
            <Image
              source={require('../../assets/icons/leafs.png')}
            />
          </View>
        </View>
      </Fragment>
    );
  }
}

export default SplashScreen;
