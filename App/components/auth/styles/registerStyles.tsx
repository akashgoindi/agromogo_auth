import { StyleSheet, Dimensions } from "react-native";

const { width } = Dimensions.get("window");

export const styles = StyleSheet.create({

  container: {
    backgroundColor: "#fff",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    height: width / 8,
    paddingLeft: 5,
    borderBottomWidth: 1,
    borderColor: "#2A4365",
    fontFamily: 'dm-regular',
  },
  inputContainer: {
    marginBottom: 20
  },
  formWrapper: {
    width: width - 30,
    paddingVertical: 20,
    paddingHorizontal: 30,
    borderRadius: 6,
  },
  title: {
    color: "#2A4365",
    fontSize: width / 10,
    fontFamily: 'bold',
    marginBottom: 20,
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: "#2A4365",
    borderRadius: 50,
    width: width - 100,
    justifyContent: "center",
    alignItems: "center",
    elevation: 2,
    marginTop: 10
  },
  buttonTextStyles: {
    color: "#fff",
    textAlign: "center",
    fontWeight: "700",
    fontSize: width / 26,
    fontFamily: ''
  },
  picker_style: {
    height: width / 8,
    borderBottomWidth: 1,
    borderColor: "#2A4365",
    marginBottom: 20
  },
});