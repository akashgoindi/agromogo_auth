import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
// import { Picker } from '@react-native-community/picker';
import { Actions } from 'react-native-router-flux';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  BackHandler,
  StatusBar,
  ActivityIndicator,
  KeyboardAvoidingView,
} from "react-native";

import{ states, cities } from "../../data";
import { toast } from '../../handlers/toastHandler';
import { ScrollView } from 'react-native-gesture-handler';
import { styles } from './styles/registerStyles';
import { handleBackPress } from '../../handlers/backHandler';

import { register, clearAuthMessage } from "../../actions/auth/index";

const { width } = Dimensions.get("window");

interface IRegisterProps {
  access_token: string | undefined,
  register: Function,
  clearAuthMessage: Function,
  message: string,
  loader: boolean,
}

interface IRegisterState {
  access_token: string | undefined,
  backClickCount: number,
  name: string,
  email: string,
  contact: string,
  password: string,
  confirmPassword: string,
  state: string,
  state_id: number,
  city: string,
  loader: boolean,
  [key: string]: any,
}

class RegisterPage extends Component<IRegisterProps, IRegisterState> {
  backHandler:any;
  constructor(props: IRegisterProps) {
    super(props);
    this.state = {
      access_token: undefined,
      backClickCount: 0,
      name: "akash",
      email: "akash@gmail.comm",
      contact: "9999199900",
      password: "123456789",
      confirmPassword: "123456789",
      state: "",
      city: "",
      state_id: 0,
      loader: false,
    };
  }

  componentDidMount = () => {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => handleBackPress("login"));
  }

  componentDidUpdate = (prevProps: IRegisterProps) => {
    const { access_token, message, clearAuthMessage, loader } = this.props;
    if (message !== "") {
      toast(message);
      this.setState({ loader: loader })
      clearAuthMessage();
    }
    if (access_token && access_token !== prevProps.access_token) {
      this.setState({ access_token }, () => { Actions.home() });
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  handleText = (data:string, key: string) => {
    if(key === "email") {
      data = data.toLowerCase();
    }
    this.setState({
      [key] : data
    })
  }

  onCityChange = (city: any) => {
    this.setState({
      city
    })
  };

  // renderCities = (cities: any) => {
  //   let { state_id, city } = this.state;
  //   let allCities = cities.filter((city: any) => city.city_id === state_id);
  //   return (
  //     <View style={styles.picker_style}>
  //       <Picker
  //         selectedValue={this.state.city}
  //         style={{ height: 50, width: width - 100 }}
  //         onValueChange={(value) => this.onCityChange(value)}>
  //         <Picker.Item label="Select Your City" color='#CBD5E0' value="" />
  //         {
  //           allCities.map((city: any, index: string) =>
  //             <Picker.Item key={index} label={`${city.city_name}`} value={`${city.city_name}`} />
  //           )
  //         }
  //       </Picker>
  //     </View>
  //   )
  // };

  onStateChange = (value: any) => {
    const test = states.find(element => {
      if (element.state_name === value) return element;
      return null;
    })
    if (!!test) {
      const state_id = test.state_id;
      this.setState({
        state: value,
        state_id
      })
    }
  };

  registerUser = () => {
    const { name, email, password, contact, confirmPassword, state, city } = this.state;
    // if (email && password && contact && name && state && city) {
      if (email && password && contact && name) {
      if(contact.length !== 10) {
        toast("Invalid contact number");
        return
      }
      if(password !== confirmPassword) {
        toast("Password Mismatch");
        return;
      }
      this.setState({ loader: true });
      this.props.register({ name, email, password, contact, state, city });
    } else {
      toast("Please provide all the details");
    }
  }

  renderSignup = () => {
    const { email, password, name, contact, confirmPassword } = this.state;
    return (
      <View>
        <Text style={styles.title}>Register</Text>
        <View style={styles.inputContainer}>
          <View>
            <TextInput
              style={styles.input}
              placeholder="Full Name"
              value={name}
              onChangeText={data => this.handleText(data, "name")}
            />
          </View>
        </View>
        <View style={styles.inputContainer}>
          <View>
            <TextInput
              style={styles.input}
              placeholder="Email Address"
              value={email}
              onChangeText={data => this.handleText(data, "email")}
            />
          </View>
        </View>
        <View style={styles.inputContainer}>
          <View>
            <TextInput
              style={styles.input}
              placeholder="10 Digit Contact Number"
              value={contact}
              maxLength={10}
              keyboardType={'numeric'}
              onChangeText={data => this.handleText(data, "contact")}
            />
          </View>
        </View>

        {/* <View>
          <View style={styles.picker_style}>
            <Picker
              selectedValue={this.state.state}
              onValueChange={(value) => this.onStateChange(value)}>
              <Picker.Item label="Select Your State" color='lightgray' value="" />
              {
                states.map((state, index) =>
                  <Picker.Item key={index} label={`${state.state_name}`} value={`${state.state_name}`} />
                )}
            </Picker>
          </View>
        </View>

        {
          !!this.state.state_id ?
            <View>
              <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginBottom: 10 }}>
                {this.renderCities(cities)}
              </View>
            </View>
            :
            <View>
              <View style={styles.picker_style}>
                <Picker
                  enabled={false}
                  selectedValue="">
                  <Picker.Item label="Select State First" color='lightgray' value="" />
                </Picker>
              </View>
            </View>
        } */}

        <View style={styles.inputContainer}>
          <View style={{ position: 'relative' }}>
            <TextInput
              style={styles.input}
              placeholder="Password"
              secureTextEntry={true}
              value={password}
              onChangeText={data => this.handleText(data, "password")}
            />
          </View>
        </View>
        <View style={styles.inputContainer}>
          <View style={{ position: 'relative' }}>
            <TextInput
              style={styles.input}
              placeholder="Confirm Password"
              secureTextEntry={true}
              value={confirmPassword}
              onChangeText={data => this.handleText(data, "confirmPassword")}
            />
          </View>
        </View>
        {
          !this.state.loader ?
            <TouchableOpacity
              style={styles.button}
              onPress={() => { this.registerUser() }}>
              <Text style={styles.buttonTextStyles}>Register</Text>
            </TouchableOpacity>
            : <TouchableOpacity
              style={styles.button}>
              <ActivityIndicator color="#fff" />
            </TouchableOpacity>
        }
        <TouchableOpacity
          style={{ justifyContent: "center", alignItems: "center", marginTop: 10, paddingVertical: 10 }}
          onPress={() => { Actions.login() }}>
          <Text>Have an account?  <Text style={{ color: "#2A4365", fontWeight: "700", fontSize: 14 }}>Login</Text></Text>
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    return (
      <Fragment>
        <StatusBar backgroundColor="#2A4365" barStyle="light-content" />
          <ScrollView>
            <View style={styles.container}> 
              <View style={styles.formWrapper}>
                {this.renderSignup()}     
              </View>
            </View>
          </ScrollView>
      </Fragment>
    );
  }
}

const mapStateToProps = ({ auth }: any) => {
  return ({
    access_token: auth.access_token,
    message: auth.message,
    loader: auth.loading.register
  });
}

export default connect(
  mapStateToProps, 
  {
    register,
    clearAuthMessage
  }
)(RegisterPage);

