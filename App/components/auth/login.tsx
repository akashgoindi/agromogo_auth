import React, { Component, Fragment } from 'react';
import { Actions } from 'react-native-router-flux';
import { connect } from "react-redux";
import {
  View,
  Text,
  BackHandler,
  TextInput,
  TouchableOpacity,
  StatusBar,
  ActivityIndicator,
  KeyboardAvoidingView,
} from "react-native";

import { toast } from '../../handlers/toastHandler';
import { styles } from "./styles/loginStyles";
import { handleBackPress } from "../../handlers/backHandler";

import { login, clearAuthMessage } from "../../actions/auth/index";

interface ILoginProps { 
  access_token: string | undefined,
  login: Function,
  clearAuthMessage: Function,
  message: string,
  loader: boolean,
}

interface ILoginState {
  access_token: string | undefined,
  backClickCount: number,
  email: string,
  password: string,
  loader: boolean,
}

class LoginPage extends Component<ILoginProps, ILoginState> {
  backHandler: any;
  constructor(props: ILoginProps) {
    super(props);
    this.state = {
      backClickCount: 0,
      access_token: undefined,
      loader: false,
      email: "amit@uniqubic.com",
      password: "admin1"
    };
  }

  componentDidMount = () => {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => handleBackPress("login"));
  }

  componentDidUpdate = (prevProps: ILoginProps) => {
    const { access_token, message, clearAuthMessage, loader } = this.props;
    if( message !== "" ) {
      toast(message);
      this.setState({ loader: loader})
      clearAuthMessage();
    }
    if (access_token && access_token !== prevProps.access_token) {
      this.setState({access_token}, () => {Actions.home()});
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  handleEmail = (data: string) => {
    let value = data.toLowerCase();
    this.setState({
      email: value
    });
  };

  handlePassword = (data: string) => {
    this.setState({
      password: data
    });
  };

  loginUser = () => {
    const { email, password } = this.state;
    if (email && password) {
      this.props.login(email, password);
      this.setState({ loader: true});
    } else {
      toast("Please provide all the details");
    }
  }

  renderLogin = () => {
    const { email , password } = this.state;
    return(
      <View>
        <Text style={styles.title}>Login</Text>
        <View style={styles.inputContainer}>
          <View>
            <TextInput
              style={styles.input}
              placeholder="Email Address"
              value={email}
              onChangeText={data => this.handleEmail(data)}
            />
          </View>
        </View>
        <View>
          <View style={styles.inputContainer}>
            <View style={{ position: 'relative' }}>
              <TextInput
                style={styles.input}
                placeholder="Password"
                secureTextEntry={true}
                value={password}
                onChangeText={data => this.handlePassword(data)}
              />
            </View>
          </View>
        </View>
        <View>
          {
            !this.state.loader ?
              <TouchableOpacity
                style={styles.button}
                onPress={() => { this.loginUser() }}
              >
                <Text style={styles.buttonTextStyles}>Login</Text>
              </TouchableOpacity>
              : <TouchableOpacity
                style={styles.button}
              >
                <ActivityIndicator color="#fff" />
              </TouchableOpacity>
          }
        </View>
          <TouchableOpacity 
            style={{justifyContent: "center", alignItems: "center", marginTop: 10, paddingVertical: 10}}
            onPress={() => { Actions.register() }}>
          <Text>New here?  <Text style={{ color: "#2A4365", fontWeight: "700", fontSize: 14}}>Register</Text></Text>
          </TouchableOpacity>
      </View>
    )
  }

  render() {
    return (
      <Fragment>
        <StatusBar backgroundColor="#2A4365" barStyle="light-content" />
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
          <View style={styles.formWrapper}>
            {this.renderLogin()}
          </View>
        </KeyboardAvoidingView>
      </Fragment>
    );
  }
}

const mapStateToProps = ({auth}: any) => {
  return({
    access_token: auth.access_token,
    message: auth.message,
    loader: auth.loading.login
  });
}

export default connect(
  mapStateToProps,
  { 
    login, 
    clearAuthMessage
  }
)(LoginPage);
