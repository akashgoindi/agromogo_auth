import React from "react";
import { Router, Stack, Scene, Drawer, Actions } from "react-native-router-flux";

import Splash from "./components/splash/splash";
import Login from "./components/auth/login";
import Register from "./components/auth/register";
import Home from "./components/home/index";
import Calendar from "./components/calendar/calendar";

const Routers = () => (
    <Router>
			<Stack key="root">
				<Scene key="splash" type="reset" component={Splash} hideNavBar={true} initial />
				<Scene key="login" type="reset" component={Login} hideNavBar={true} />
				<Scene key="register" type="reset" component={Register} hideNavBar={true} />
				<Scene key="home" type="reset" component={Home} hideNavBar={true} />
				<Scene key="calendar" type="reset" component={Calendar} hideNavBar={true} />		
			</Stack>
    </Router>
);

export default Routers;
