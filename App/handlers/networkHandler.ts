import {getToken} from "./tokenHandler";
import { toast } from "./toastHandler";
import NetInfo from "@react-native-community/netinfo";
import { base_url } from "../config";

let internetStatus = true;

// First Time Fetch Status
NetInfo.fetch().then(state => {
  internetStatus = state.isConnected;
});

// Subscribe
NetInfo.addEventListener(state => {
  internetStatus = state.isConnected;
});

const ACCEPT_METHODS = ["GET", "DELETE", "POST", "PUT"];
const AUTH_FAIL_CODES = [401];
const ERROR_MESSAGE = "Something went wrong. Please try again in some time";

function validParams(params) {
  return typeof params === "object" && Object.keys(params).length > 0;
}

function getQueryString(params) {
  const esc = encodeURIComponent;
  return Object.keys(params)
    .map(k => `${esc(k)}=${esc(params[k])}`)
    .join("&");
}

function getHeaderJson() {
  const token = getToken();
  const headers = {
    "Content-Type": "application/json",
    "Accept-Encoding": "gzip",
  };

  if (token) {
    headers["authorization"] = token;
  }

  return headers;
}

function getFullURL(method = "GET", path = "", params = {}) {
  if (method && ACCEPT_METHODS.includes(method) && path) {
    let url = base_url + path;
    if (method === "GET") {
      if (validParams(params)) {
        let qs = "";
        qs = `?${getQueryString(params)}`;
        url += qs;
      }
    } else if (method === "DELETE") {
      if (validParams(params)) {
        let qs = "";
        qs = `?${getQueryString(params)}`;
        url += qs;
      }
    }
    // if (method === "POST") {}
    // if (method === "PUT") {}
    return url;
  }
  return null;
}

function getOptions(method = "GET", params = {}) {
  if (method && ACCEPT_METHODS.includes(method)) {
    const options = {
      method,
      headers: getHeaderJson(),
    };
    // if (method === "GET") {}
    // if (method === "DELETE") {}
    if (method === "POST") {
      if (validParams(params)) {
        options.body = JSON.stringify(params);
      }
    } else if (method === "PUT") {
      if (validParams(params)) {
        options.body = JSON.stringify(params);
      }
    }
    return options;
  }
  return null;
}

function sendError(status, message, responseJson = {}) {
  const err = { status, message, responseJson };
  throw err;
}

export const checkInternet = () => {
  if (internetStatus) {
    return true;
  }
  return false;
};

export const requestApiAsync = async ({
  path,
  method = "GET",
  params = {}
}) => {
  const url = getFullURL(method, path, params);
  const options = getOptions(method, params);

  if (!checkInternet()) {
    throw sendError(408, "Internet Not Connected");
  }

  if (!url || !options) {
    throw sendError(400, "Bad Request");
  }
  const response = await fetch(url, options);
  if (!response.ok) {
    if (AUTH_FAIL_CODES.includes(response.status)) {
      throw sendError(401, "Authentication Failed");
    } else {
      throw sendError(response.status, ERROR_MESSAGE);
    }
  }
  const contentType = response.headers.get('content-type'); 
  if (contentType && contentType.includes('application/json')) { 
    const responseJson = await response.json();
    if (responseJson.error) { 
      const { message: responseMessage } = responseJson;
      // Handles Server Error Messages
      if (responseMessage) {
        toast(responseMessage);
        throw sendError(400, responseMessage, responseJson);
      }
      // Handles errors not handled by server
      throw sendError(500, ERROR_MESSAGE, responseJson);
    }
    return responseJson;
  } else if (contentType && contentType.includes('text/plain')) {
    return await response.text();
  }
};