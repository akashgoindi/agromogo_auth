const error_400 = async () => {
return;
}

const error_401 = async () => {
return;
}

const error_404 = async () => {
return;
}

export const error_handler = (status: number) => {
  if (status === 401) { // Unauthorized
    error_401();
  } else if (status === 400) {
    error_400();
  } else if (status === 404){
    error_404(); 
  } else {
    return null;
  }
}
