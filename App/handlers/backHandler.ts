import { Actions } from "react-native-router-flux";
import {Alert, BackHandler } from "react-native";

let clicks = 0;

const handleExit = () => {
  clicks = clicks + 1;
  if (clicks === 2) {
    Alert.alert(
      "Exiting App",
      "Are you sure you want to exit?",
      [
        {
          text: "OK",
          onPress: () => {BackHandler.exitApp() },

        },
        {
          text: "CANCEL",
          style: 'cancel',
          onPress: () => { }
        },
      ],
      { cancelable: false },
    );
  }
  setTimeout(() => {
    console.log("Inside")
    clicks = 0;
  }, 1000);
  return false;
};

export const handleBackPress = (currentScreen = "") => {
  // currentScreen may be used for diff actions in future ...
  return handleExit();
};


