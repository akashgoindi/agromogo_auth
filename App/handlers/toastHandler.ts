import { ToastAndroid } from "react-native"; 

// Only message will be passed as param as we want toasts to be uniform in entire app. 
//No need to change sizes.Color theme will be considered later in time.
export const toast = (message: string) => {
  ToastAndroid.showWithGravity(
    message,
    ToastAndroid.LONG,
    ToastAndroid.TOP,
    25,
    50
  );
}