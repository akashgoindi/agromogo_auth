import AsyncStorage from "@react-native-community/async-storage";

export const setToken = async (accessToken = "") => {
  if (accessToken && accessToken !== "undefined") {
    let token = accessToken.split(" ")[1];
    try {
      await AsyncStorage.setItem("access_token", token);
    } catch (error) {
      throw new Error("Something went wrong!")
    }
  }
}

export const getToken = async () => {
  let token = "";
  try {
    token = await AsyncStorage.getItem('access_token');
  } catch (error) {} // No need to handle error
  return token;
}

export const clearAsyncStorage = () => {
  AsyncStorage.clear();
}