import { LOGIN, LOGOUT, REGISTER, CLEAR_AUTH_MESSAGE } from "../actionTypes";
import INITIAL_STATE from "./models/auth";

export default (state = INITIAL_STATE, action: any) => {
  let data = action.payload && action.payload.data ? action.payload.data : {};
  let message = action.payload && action.payload.message ? action.payload.message : "";
  let status = action.payload && action.payload.status ? action.payload.status : undefined;

  switch (action.type) {
    case LOGIN:
      if (status === "success") {
        let token: string = data.token ? data.token.split(" ")[1] : undefined;
        return { ...state, access_token: token, loading: {...state.loading, login: false}, message };
      } else if (status === "error") {
        return ({ ...state, loading: { ...state.loading, login: false }, message });
      }
      return ({ ...state, loading: { ...state.loading, login: true }});

    case REGISTER:
      if (status === "success") {
        let token: string = data.token ? data.token.split(" ")[1] : undefined;
        return { ...state, access_token: token, loading: { ...state.loading, register: false }, message };
      } else if (status === "error") {
        return ({ ...state, loading: { ...state.loading, register: false }, message });
      }
      return ({ ...state, loading: { ...state.loading, register: true } });

    case CLEAR_AUTH_MESSAGE:
      return { ...state, message: "" };
    
    case LOGOUT:
      return INITIAL_STATE ;
    
    default:
      return state;
  }
};
