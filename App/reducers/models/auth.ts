interface InitialState {
  message: string,
  access_token: undefined | string,
  loading: {
    login: boolean;
    register: boolean;
  }
}

const INITIAL_STATE: InitialState = {
  message: "",
  access_token: undefined,
  loading: {
    login: false,
    register: false
  }
}

export default INITIAL_STATE;