import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import React from "react";
import {AppRegistry} from 'react-native';
import App from './App/index';
import {name as appName} from './app.json';

// AppRegistry.registerComponent(appName, () => { return (<NavigationContainer>App</NavigationContainer>)});
AppRegistry.registerComponent(appName, () => App);